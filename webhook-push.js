const { exec } = require('child_process')

const { getJSON } = require('./util')

module.exports = (req) => {
    return getJSON(req).then(json => {
        if (json.ref === 'refs/heads/master') {
            console.log('发布上线', new Date())

            const command = [
                'cd /root/project/webApp',
                'git fetch --all',
                'git reset --hard origin/master',
                'git pull',
                'yarn',
                'npm run pro:sm',
            ].join(' && ')
            
            exec(command)
            return
        } else if (json.ref === 'refs/heads/develop') {
            exec([
                'cd /root/project/webapp-jsdoc',
                'npm run update'
            ].join(' && '))
            return
        }
    })
}
