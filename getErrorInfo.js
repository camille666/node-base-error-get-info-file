const fs = require('fs')
const http = require('http')
const https = require('https')
const { execSync } = require('child_process')
const SourceMapConsumer = require('source-map').SourceMapConsumer

const { getJSON } = require('./util')

// 发送钉钉消息
const sendText = (content, at) => {                                                 
    const req = https.request(
        'https://oapi.dingtalk.com/robot/send?access_token=8c5c9a5a5ee38652738ddebf9ffaeec7dac325f7a4c93791aa9f63888c5cbc15',
        {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            }
        },
        () => {}
    )

    const findReq = http.request(
        'http://localhost:8014/api/people/find',
        {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
            }
        },
        (resp) => {
            getJSON(resp).then(res => {
                const author = res.data.find(item => item.names.includes(at.trim()))
    
                req.write(JSON.stringify({
                    msgtype: 'text',
                    text: {
                        content
                    },
                    at: {
                        atMobiles: author ? [author.phone]: [],
                        isAtAll: false,
                    }
                }))
            
                req.end()
            })
        }
    )
    
    findReq.write(JSON.stringify({}))
    findReq.end()
}

// 获取代码提交信息
const getCodePushInfo = (source, line, column) => {
    let log = ''

    try {
        log = execSync(`cd /root/project/webApp && git blame ${source} -L ${line},${column}`).toString()
    } catch (err) {
        return {}
    }

    const result = log.match(/(.*)\ \((.*)\ (\d+-\d+-\d+\ \d+:\d+:\d+).*\).*/)
    const [, hash, author, date] = result

    return {
        hash,
        author,
        date,
    }
}

// 获取几条错误代码附近信息
const getErrorLineCode = (source, line) => {
    const lines = fs.readFileSync(`/root/project/webApp/${source}`).toString().split('\n')
    const content = lines.slice(line > 0 ? line - 1 : line, line + 6).join('\n')

    return content
}

// 获取map信息
const getMapInfo = ({ path, line, column }) => {
    return new Promise(resolve => {
        const mapPath = `/root/project${path}.map`

        if (!fs.existsSync(mapPath)) {
            resolve(null)
            return
        }

        const fileContent = fs.readFileSync(mapPath, 'utf8')

        SourceMapConsumer.with(JSON.parse(fileContent), null, consumer => {
            // 输出map的错误信息
            const result = consumer.originalPositionFor({
                line, // 行号
                column, // 列号
            })

            resolve(result.source ? result : null)
        })
    })
}

module.exports = (req) => {
    getJSON(req).then(json => {
        const JSRuntimeError = new Function(`return ${json.message.split('errorByFront:')[1]}`)()
        const { UA, url, screen } = JSRuntimeError || {}
        const { message, stack } = JSRuntimeError.JSRuntimeError || {}
        const match = stack.match(/(\/webApp\/dist\/.*):(\d+):(\d+)/)

        if (match) {
            const [ , sourceURL, line, column ] = match;
            
            getMapInfo({ path: sourceURL, line: Number(line), column: Number(column) }).then(res => {
                if (!res) {
                    return
                }

                const { line, column } = res
                const source = res.source.replace('webpack:///', '')
                
                if (source.includes('node_modules/')) {
                    return
                }

                const { hash, author, date } = getCodePushInfo(source, line, column)
                const content = getErrorLineCode(source, line)

                sendText([
                    `收到一条错误: ${url}`,
                    '',
                    `信息：${message}`,
                    `源文件：${source}`,
                    `代码行：${line}`,
                    `代码列：${column}`,
                    `commitHash：${hash}`,
                    '源代码',
                    content,
                    `代码提交者：${author}`,
                    `代码提交时间：${date}`,
                    `屏幕大小：${screen}`,
                    `浏览器信息：${UA}`,
                ].join('\n'), author)
            })
        }
    })
}
