module.exports.getJSON = req => {
    return new Promise(resolve => {
        let str = ''

        req.on('data', d => str += d)
        req.on('end', () => resolve(JSON.parse(str)))
    })
}
