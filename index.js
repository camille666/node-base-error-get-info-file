const http = require('http')
const webhookPush = require('./webhook-push')
const findSource = require('./getErrorInfo')

const router = {
    '/webhook/push': (req) => webhookPush(req),
    '/getErrorInfo': (req) => findSource(req),
}

http.createServer((req, res) => {
    router[req.url] && router[req.url](req, res)

    res.end()
}).listen(8235)
